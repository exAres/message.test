<script src="/js/messages.js"></script>

<div class='menu'>
<h2>Administrating users</h2>

<h3>Menu:</h3>
<a href="/">Home</a>
<br><br>
<a href="/user/profile/create/true">Create new user</a>
</div>
<div class='right-block messages'>
<div>
	<table class="container">
		<tr class='filter-tr'>
			<td>Name:<input type="hidden" value=""></td>
			<td>Login:<input type="hidden" value=""></td>
			<td>Role:<input type="hidden" value=""></td>
			<td>Status:<input type="hidden" value=""></td>
		</tr>
		<?php foreach ($users as $user) { ?>
			<tr class='list-tr user-list-edit' user="<?php echo $user->id;?>">
				<td><?php echo $user->name;?></td>
				<td><?php echo $user->login;?></td>
				<td><?php echo $user->role;?></td>
				<td><?php echo $user->status;?></td>
			</tr>
		<?php } ?>
	</table>
	<?php //else: ?>
<!-- 		<div style="text-align: center;">
			<h1>No searching results</h1>
		</div> -->
</div>