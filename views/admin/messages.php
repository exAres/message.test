<div class='menu'>
<h2>Administrating messages</h2>

<h3>Menu:</h3>
<a href="/">Home</a>
</div>
<div class='right-block messages'>
<div>
	<table class="container">
		<tr class='filter-tr'>
			<td>From:<input type="hidden" value=""></td>
			<td>To:<input type="hidden" value=""></td>
			<td>Text:<input type="hidden" value=""></td>
			<td>Created:<input type="hidden" value=""></td>
			<td>Status:<input type="hidden" value=""></td>
		</tr>
		<?php foreach ($messages as $message) { ?>
			<tr class='list-tr'>
				<td><?php echo $message->sender_id;?></td>
				<td><?php echo $message->target_id;?></td>
				<td><?php echo $message->text;?></td>
				<td><?php echo $message->created;?></td>
				<td><?php echo $message->status;?></td>
			</tr>
		<?php } ?>
	</table>
	<?php //else: ?>
<!-- 		<div style="text-align: center;">
			<h1>No searching results</h1>
		</div> -->
</div>