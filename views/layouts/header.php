<!--<base href="http://ex.mvc/"/>-->
<head>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
<!--CSS-->
<link type="text/css" rel="stylesheet" href="/css/general.css" />
<!--JavaScript-->
<script src="/js/jquery.js"></script>
<script src="/js/main.js"></script>
<div class='flash-message'>
<span id='flash_message_text'>
</span>
</div>
<?php $flash = MVC::app()->getFlash(); 
	if($flash != null): ?>
	<script type="text/javascript">
		$(function(){
			flashMessage('<?php echo $flash['class'];?>','<?php echo $flash['message'];?>');
		});
	</script>
<?php endif; ?>
<title>Messages test site</title>
<div class='head'>
<h1 style="width: 300px;"><a class='no_href' href='/' style="position: absolute;top: 10;">Messages test site (Дацюк Максим)</a></h1>
<?php if(MVC::app()->user->role != 'guest'):
	$new_messages = SystemFunctions::getNewMessages();?>
	<div style="margin-top: -10;">
		<button <?php if($new_messages < 1): ?>disabled<?php else:?>class="blinkdiv"<?php endif;?> onclick="document.location.href = '/user/messages/filter/new'" type="button" style="width: 200px;height: 30px;font-size: 134%;margin-top: -16px;border-radius: 10px;">New messages(<?php echo $new_messages;?>)</button>
		<button onclick="document.location.href = '/user/write'" type="button" style="width: 200px;height: 30px;font-size: 134%;margin-top: -16px;background-color:rgb(133, 135, 140);  border-radius: 10px;">Write message</button>
	</div>
<?php endif;?>
<div class="login">
	<form action="/user/login" method="post" enctype="multipart/form-data">
	<?php if(MVC::app()->user->role == 'guest'):?>
	<input class='input_dark' placeholder="Login" name="name" type="text" required="true">
	<input class='input_dark' placeholder="Password" name="pass" type="password" required="true">
	<div style="margin-top: 4px;">
		<input class='button' type="submit" name="submit" value="Login" onclick="" >
		<!-- <input class='button' style="float:right;margin-right:65px;width: 166px;" value="Registration" onclick='add_overlay("_registration");'> -->
	</div>
	<?php else:?>
	<input name="logout" type="hidden">
	<h4 style='margin-bottom: 0px;margin-top: -5px;' >Dear <span id="user_name"><?php echo(MVC::app()->user->name);?></span> welcome to my site.</h4>
	<input class='button' style='width: 80px;height: 20px;top: 10px;' type="submit" name="submit" value="Logout">
	<?php endif;?>
	</form>
</div>
</div>
</head>
<body class='body'>