<script src="/js/messages.js"></script>
<input type="hidden" id='request_uri' value="<?php echo($_SERVER['REQUEST_URI']);?>">
<div class='menu'>
<h2>Messages
<button class="button" style="float:right;margin-right: 10px;background-color:rgb(100,155,100);height:30px;" onclick="refresh_messages();" >Refresh</button>
</h2>

<h3>Menu:</h3>
<a href="/">Home</a>
</br></br>
<a href="/user/messages/filter/all"> - All</a>
</br>
<a href="/user/messages/filter/new"> - New</a>
</br>
<a href="/user/messages/filter/sended"> - Sended</a>
</div>
<div class='right-block messages'>
<div id='messages_container'>
	<?php if(count($messages) > 0): ?>
	<table class="container">
		<?php foreach ($messages as $message) { ?>
		<tr user="<?php echo $message['u_id'];?>" class="message-tr inner <?php if($message['status'] == 'new'): echo('new'); endif;?>">
			<td class="left"><?php if($message['sender_id'] == MVC::app()->user->id) echo("<b>TO: </b>");echo $message['name'];?><br><span class="data-span"><?php echo $message['created'];?><span></td>
			<td class="right"><?php echo nl2br($message['text']);?></td>
		</tr>
		<?php } ?>
	</table>
	<?php else: ?>
		<div style="text-align: center;">
			<h1>No searching results</h1>
		</div>
	<?php endif; ?>
</div>
</div>