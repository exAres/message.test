<script src="/js/messages.js"></script>
<input type="hidden" id='request_uri' value="<?php echo($_SERVER['REQUEST_URI']);?>">
<div class='menu'>
<h2>Write message
<button class="button" style="float:right;margin-right: 10px;background-color:rgb(100,155,100);height:30px;" onclick="refresh_messages('<?php echo($_SERVER['REQUEST_URI']);?>');" >Refresh</button>
</h2>

<h3>Menu:</h3>
<a href="/">Home</a>
<br>
<a href="/user/messages">My messages</a>
</div>
<div class='right-block messages' style="margin-bottom: 0px;">
	<div class="write-block">
		<form id="user_sendmessage" action="/user/sendmessage" method="post">
			<h2 style="display:block;margin-top: 0px;margin-bottom: 0px;">Write your message:</h2>
			<textarea id='message_text' name="Message[text]"></textarea>
			<div class='user-selector-block'>
				<label><b>TO:</b></label>
				<?php if($target != null): ?>
					<div id='sending_user' value='<?php echo $target->id;?>' onclick='add_overlay("users_list");'><?php echo $target->name;?></div>
					<input id='message_target' type="hidden" value="<?php echo $target->id;?>" name="Message[target_id]"/>
				<?php else: ?>
					<div id='sending_user' value='0' onclick='add_overlay("users_list");'>▼ Select user ▼</div>
					<input id='message_target' type="hidden" value="0" name="Message[target_id]"/>
				<?php endif; ?>
				<button type="submit" id='sending_button' <?php if($target == null): ?>disabled<?php endif; ?>>Send message</button>
			</div>
		</form>
	</div>
</div>
<?php if($target != null): ?>
<div class='right-block messages'>
	<div id='messages_container'>
		<?php if(count($messages) > 0): ?>
		<table class="container">
			<?php foreach ($messages as $message) { ?>
			<tr user="<?php echo $target->id; ?>" class="inner <?php if($message['status'] == 'new'): echo('new'); endif;?> <?php if($message['sender_id'] == MVC::app()->user->id): echo('my-messages'); endif;?>">
				<td class="left"><?php 
				if($message['sender_id'] == $target->id):
					echo $target->name;
				else: 
					echo MVC::app()->user->name;
				endif;
				?>
				<br><span class="data-span"><?php echo $message['created'];?><span></td>
				<td class="right"><?php echo nl2br($message['text']);?></td>
			</tr>
			<?php } ?>
		</table>
		<?php else: ?>
			<div style="text-align: center;">
				<h1>No chatting with this user</h1>
			</div>
		<?php endif; ?>
	</div>
</div>
<?php endif; ?>

<div id="users_list" class='modal users-list'>
    <h2 style='text-align: center;font-size: 40px;margin-top: 0px;margin-bottom: 10px;'>Select user:</h2>
    <?php foreach ($users as $user) { ?>
    	<div class="user-select-id" value="<?php echo $user->id;?>"><?php echo $user->name;?></div>
    <?php } ?>
</div>
