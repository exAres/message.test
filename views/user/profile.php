<?php if($model == null){ $model = MVC::app()->user;}?>
<div class='menu'>
<h2>Profile</h2>

<h3>Menu:</h3>
<a href="/">Home</a>
</div>
<div class='right-block messages'>
<div>
	<form id="user_sendmessage" action="/user/profile" method="post">
		<input type="hidden" name="User[id]" value="<?php echo $model->id; ?>">
		<table class="container profile-edit">
			<tr>
				<td><h2>Username:</h2></td>
				<td><input type="text" name="User[name]" required value="<?php echo $model->name; ?>"></td>
			</tr>
			<tr>
				<td><h2>Login:</h2></td>
				<td><input type="text" name="User[login]" required value="<?php echo $model->login; ?>"></td>
			</tr>
			<tr>
				<td><h2>Password:</h2></td>
				<td><input type="password" name="User[password]" value=""></td>
			</tr>
			<tr>
				<td><h2>Repeat:</h2></td>
				<td <?php if(isset($model->error['repeat'])): ?>class='error'<?php endif; ?>>
					<input type="password" name="User[repeat]" value="">
					<?php if(isset($model->error['repeat'])): ?><label class='error'><?php echo $model->error['repeat'];?></label><?php endif; ?>
				</td>
			</tr>
			<?php if(MVC::app()->user->role == 'admin'): ?>
				<tr>
					<td><h2>Status:</h2></td>
					<td><select name="User[status]">
					<?php foreach ($model->setStatus() as $key => $value) { ?>
						<option <?php if($key == $model->status): ?>selected<?php endif;?> value="<?php echo $key;?>"><?php echo $value;?></option>
					<?php } ?>
					</select></td>
				</tr>
				<tr>
					<td><h2>Role:</h2></td>
					<td><select name="User[role]">
					<?php foreach ($model->setRole() as $key => $value) { ?>
						<option <?php if($key == $model->role): ?>selected<?php endif;?> value="<?php echo $key;?>"><?php echo $value;?></option>
					<?php } ?>
					</select></td>
				</tr>
			<?php endif; ?>
			<tr>
				<td></td>
				<td><input class="button" style="font-size: 130%;margin-top: 50px;width: 300px;" type="submit" value="Save"></td>
			</tr>
		</table>
	</form>
</div>