<?php

class Messages extends DefaultModel { // Модель повідомлень

    public $table_name = 'messages';
    public $id;
    public $sender_id;
    public $target_id;
    public $text;
    public $created;
    public $status;

    public function save() { // Перевірка на наявність помилок, та збереження
        // Треба буде вдосконалити її, щоб була одна функція для всіх моделей і по всіх потрібних
        // полях пробігало + формувало одразу помилки
        if(!isset($this->error)){
            $tosave = array();
            $tosave['sender_id'] = $this->sender_id;
            $tosave['target_id'] = $this->target_id;
            $tosave['text'] = $this->text;
            $tosave['status'] = $this->status;
            if($this->id != NULL){
                parent::update($this->table_name, $tosave);
            }else{
                parent::insert($this->table_name, $tosave);
            }
            return true;
        }else{
            return false;
        }
    }

    public function getNewMessagesCount($id){ // Кількість нових повідомлень
        $count = self::model()->scriptFindAll("SELECT COUNT(*) FROM `messages` WHERE target_id = ".$id." && status = 'new' ORDER BY `created`");
        return $count[0];
    }

    public function findAllByFilter($search){ // Всі повідомлення в залежності від фільтра
        $filter = "AND m.status <> 'deleted' AND m.status <> 'disabled'";
        $user = "m.sender_id = u.id && m.target_id = ".MVC::app()->user->id;
        if(isset($search)){
            if($search == 'new'){
                $filter = "AND m.status = 'new'";
            }elseif($search == 'sended'){
                $user = "m.sender_id = ".MVC::app()->user->id." && m.target_id = u.id";
            }
        }
        return Messages::model()->select("SELECT m.id as m_id,m.status,m.text,m.created,m.sender_id,u.id as u_id,u.name FROM messages m inner join user u ON ".$user." ".$filter." ORDER BY m.created DESC");
    }

    public function findAllFromUser($id){ // Всі повідомлення в залежності від користувача
        $messages = Messages::model()->select("SELECT * FROM messages WHERE (sender_id = ".$id." OR sender_id = ".MVC::app()->user->id.") AND (target_id = ".$id." OR target_id = ".MVC::app()->user->id.") AND status <> 'deleted' AND status <> 'disabled' ORDER BY created DESC");
        return $messages;
    }

    public function MarkAsReaded($id){ // Апдейт статусу непрочитаних повідомлень
        Messages::model()->query('UPDATE messages SET status = "enabled" WHERE target_id = '.MVC::app()->user->id.' AND sender_id = '.$id);
    }

    public static function model($className = __CLASS__) { // Побудова структури для зручного звертання
        return parent::model($className);
    }

}
