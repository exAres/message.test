<?php

class User extends DefaultModel {

    public $table_name = 'user';
    public $id;
    public $password;
    public $login;
    public $name;
    public $role;
    public $status;

    public function save() { // збереження моделі
        if(!isset($this->error)){
            $tosave = array();
            $tosave['name'] = $this->name;
            $tosave['login'] = $this->login;
            $tosave['password'] = $this->password;
            $tosave['role'] = $this->role;
            $tosave['status'] = $this->status;
            if($this->id != NULL){
                parent::update($this->table_name, $tosave);
            }else{
                parent::insert($this->table_name, $tosave);
            }
            return true;
        }else{
            return false;
        }
    }

    public function findAllEnabled(){ // знайти всіх доступних користувачів
        return self::model()->scriptFindAll('select id,name from user where status = "enabled" and id <> '.MVC::app()->user->id);
    }

    public function setStatus(){ // можливі статуси
        return array(
            'enabled' => 'enabled',
            'disabled' => 'disabled',
            'deleted' => 'deleted',
        );
    }
    public function setRole(){ // можливі ролі
        return array(
            'user' => 'user',
            'admin' => 'admin',
        );
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
