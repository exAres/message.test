<?php

class AdminController extends Controller
{

    private $header = '/layouts/header';
    private $footer = '/layouts/footer';

    function render($param,$_globals = NULL)
    {
        parent::render($param, $this->header, $this->footer, $_globals);
        exit();
    }

    public function actionUsers() { // список користувачів
        if(MVC::app()->user->role == 'admin'):
            $users = User::model()->findAll();

            self::render('/admin/users',array('users' => $users));
        else:
            Route::NotEnoughRights();
        endif;
    }

    public function actionMessages() { // список повідомлень
        if(MVC::app()->user->role == 'admin'):
            $messages = Messages::model()->findAll();

            self::render('/admin/messages',array('messages' => $messages));
        else:
            Route::NotEnoughRights();
        endif;
    }


}

?>