
function add_overlay(id) {
    var overlay = "<div id='file_overlay'></div>";
    $("body").append(overlay);
    
    var options = {top: 50, overlay: 0.5, closeButton: null};
    var o = $.extend(options);
    $("#file_overlay").css({
        "display": "block",
        "opacity": 0,
        "position": "fixed",
        "z-index": 100,
        "top": "0px",
        "left": "0px",
        "height": "100%",
        "width": "100%",
        "background": "#000"});
    $("#file_overlay").fadeTo(200, o.overlay);
    $("#"+id).css({"display": "block"});
    $("#file_overlay").click(function() {
        $("#file_overlay").fadeOut(200);
        $("#"+id).css({"display": "none"});
    });
}
function close_overlay(id){
    $("#file_overlay").fadeOut(200);
    $("#"+id).css({"display": "none"});
}

function clickEvent(element) {
    var evt = document.createEvent("MouseEvents");
    evt.initMouseEvent("click", true, true, window,
        0, 0, 0, 0, 0, false, false, false, false, 0, null);
    var a = document.getElementById(element);
    a.dispatchEvent(evt);
}

function flashMessage(_class,message){
    $('#flash_message_text').html(message);
    $('.flash-message').addClass(_class);
    $('.flash-message').fadeIn(500);
    setTimeout(function(){
        $('.flash-message').fadeOut(500);
    },800);
}