$(function(){
    setInterval(function(){
        refresh_messages();
    },1000*60*10);

    $('.user-select-id').on('click',function(){
        $('#sending_button').prop('disabled',false);
        $('#sending_user').html($(this).html());
        $('#message_target').val($(this).attr('value'));
        close_overlay('users_list');
    });

    $('.message-tr').on('click',function(){
        document.location.href = '/user/write/id/'+$(this).attr('user');
    });

    $('#user_sendmessage').on('submit',function(){
        if($('#message_text').val() == ''){
            alert('Message can\'t be empty!');
            return false;
        }
    });

    $('.user-list-edit').on('click',function(){
        document.location.href = '/user/profile/id/'+$(this).attr('user');
    });

});

function refresh_messages(){
    $.post($('request_uri').val(),{},function(data){
        $('#messages_container').html($(data).find('#messages_container').html());
    });
}