<?php 
require_once "core/config.php";
require_once "core/DefaultController.php";
require_once "core/DefaultModel.php";
require_once "core/Route.php";
require_once "core/DBC.php";
require_once 'core/bootstrap.php';
require_once 'core/MVC.php';
require_once 'core/SystemFunctions.php';

Route::start();
MVC::app();
?>