<?php

class MVC {
    public $user;
    public $flash = null;

    private static $_instance;

    private function __construct() {
        session_start();
        if (isset($_SESSION['user'])) {
            $this->user = $_SESSION['user'];
            if(isset($_SESSION['flash'])){
                $this->flash = $_SESSION['flash'];
            }
        } else {
            $this->user = new User();
            $this->user->id = 0;
            $this->user->name = 'guest';
            $this->user->login = 'guest';
            $this->user->password = '';
            $this->user->role = 'guest';
        }
    }

    private function __clone() {
    }
    
    public function Login($name,$pass){
        $model = User::model()->find("`login`='".$name."' AND `password`='".$pass."'");
        if(!empty($model)){
            $this->user = $_SESSION['user'] = $model;
            return true;
        }
        else return false;
    }
    
    public function Logout(){
        session_destroy();
        $this->user->id = 0;
        $this->user->name = 'guest';
        $this->user->login = 'guest';
        $this->user->password = '';
        $this->user->role = 'guest';
    }

    public static function app() {
        if (self::$_instance) {
            self::$_instance;
        } else {
            self::$_instance = new MVC();
        }
        return self::$_instance;
    }

    public function setFlash($class,$message){
        $this->flash['class'] = $_SESSION['flash']['class'] = $class;
        $this->flash['message'] = $_SESSION['flash']['message'] = $message;
    }
    public function getFlash(){
        if(isset($this->flash['class'])){
            $fl = $this->flash;
            $this->flash = $_SESSION['flash'] = null;
            return $fl;
        }
        return null;
    }

}